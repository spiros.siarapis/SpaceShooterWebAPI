<?php namespace HighscoreAPI;

use Exception;

require_once(__DIR__ . "/manager.php");
require_once(__DIR__ . "/connection.php");

$sentName = trim($_POST["name"]);
$sentScore = trim($_POST["score"]);
$sentGameKey = trim($_POST["gameKey"]);

$games = $pdo->SelectGame(key: $sentGameKey);

if( NameVerify($sentName) &&
    ScoreVerify($sentScore) &&
    isset($sentGameKey) &&
    !empty($sentGameKey) &&
    sizeof($games) == 1)
{
    $game = $games[0];

    $gameid = $game["ID"];

    $pdo->InsertHighScore(name: $sentName, score: $sentScore, gameid: $gameid);

    print("Successfully inserted a new highscore!");
    http_response_code(200);
}
else {
    print("An error happened!");
    http_response_code(404);
}

exit;
?>