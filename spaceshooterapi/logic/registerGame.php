<?php namespace HighscoreAPI;
require_once(__DIR__ . "/../manager.php");
require_once(__DIR__ . "/../connection.php");
require_once(__DIR__ . "/../common/errors.php");

function GetNewGameKey(PDOExtend $pdo): string
{
    $key = random_str(100);

    $keyExists = sizeof($pdo->SelectGame(key: $key)) !== 0;
    if($keyExists)  // Get new key if it already exists
        $key = GetNewGameKey($pdo);

    return $key;
}


# Register a new game if the data is sent
if(isset($_POST["gameName"]) && isset($_POST["registerKey"])) {
    $gameName = htmlentities(trim($_POST["gameName"]));
    $sentRegisterKey = trim($_POST["registerKey"]);

    if(password_verify($sentRegisterKey, $registerKey)) {  // Check so that the sent registerKey matches the current registerKey
        $gamesWithName = $pdo->SelectGame(name: $gameName, limit: 1);
        
        if(sizeof($gamesWithName) == 0) {  // Check so that there are no other games called gameName
            if(NameVerify($gameName)) {  // Verify the sent gameName
                // Get a new game key 
                // and insert the game in the database
                $gameKey = GetNewGameKey($pdo);

                $pdo->InsertGame($gameName, $gameKey);
                
                echo "<p class='text-xl'>Game Key: {$gameKey}</p>";
                echo "<p class='text-xl'>Keep this key a secure!</p>";
            }
            else
                header("location: ../index.php?nameError={$gameNameNotInFormat}");
        }
        else
            header("location: ../index.php?nameError={$gameNameExistsError}");
    }
    else
        header("location: ../index.php?keyError={$registerKeyNotMatch}");
    
    exit;
}

header("location: ../index.php");
exit;
?>