<?php namespace HighscoreAPI;
require_once(__DIR__ . "/manager.php");
require_once(__DIR__ . "/connection.php");

$res = array();

$limit = PDOExtend::baseLimit;
if(isset($_GET["limit"]))
    $limit = intval(trim($_GET["limit"]));

$idvar = $_GET["id"];
$id = -1;
if(isset($idvar) && is_numeric($idvar))
    $id = intval($idvar);

$gameidvar = $_GET["gameid"];
$gameid = -1;
if(isset($gameidvar) && is_numeric($gameidvar))
    $gameid = intval($gameidvar);

$playernamevar = $_GET["playername"];
$playername = null;
if(isset($playernamevar))
    $playername = $playernamevar;

$gamenamevar = $_GET["gamename"];
$gamename = null;
if(isset($gamenamevar))
    $gamename = $gamenamevar;

$scorevar = $_GET["score"];
$score = -1;
if(isset($scorevar) && is_numeric($scorevar))
    $score = intval($scorevar);


$res = $pdo->SelectHighScore(
    gameid: $gameid, 
    id: $id, 
    name: $playername, 
    gamename: $gamename, 
    score: $score, 
    limit: $limit);


$res = json_encode($res);
print($res);
exit;
?>