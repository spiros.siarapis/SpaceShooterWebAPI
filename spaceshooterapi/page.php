<?php namespace HighscoreAPI;
require_once(__DIR__ . "/manager.php");
MakeInaccessible(__FILE__);

/* Font Size
** h1 => text-4xl
** h2 => text-4xl
** h3 => text-2xl
** h4 => text-xl
** input => text-xl
** p => text-xl
*/
/* Page template for all the sites */
$html = <<<EOD
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
        
        <title>{$title}</title>
    </head>
    <body>
        <div class="container mx-auto  border-r-2 border-l-2 border-solid border-black  min-h-screen {$bodyclasses}">
            {$header}

            {$htmlContents}
            
            {$footer}
        </div>
    </body>
</html>
EOD;

echo $html;
exit;
?>