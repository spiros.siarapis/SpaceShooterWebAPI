<?php namespace HighscoreAPI;
require_once(__DIR__ . "/../manager.php");
MakeInaccessible(__FILE__);


use PDO;
use Exception;

class PDOExtend {
    private $pdo;
    public PDOSettings $settings;
    
    const baseLimit = 25;
    
    function __construct(PDOSettings $settings)
    {
        $this->settings = $settings;
        
        try {
            $this->pdo = new PDO(
                $settings->dsn,
                $settings->user,
                $settings->pwd,
                $settings->options);
        }
        catch(Exception $e) {
            die("Could not connect to the database:</br>{$e}");
        }
    }

    function CallStatement(string $sql, array $opt = []): mixed {
        try {
            $stm = $this->pdo->prepare($sql);
            $stm->execute($opt);
            return $stm->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(Exception $e) {
            throw $e;
        }
    }

    /// HighScores //
    // SELECT
    function SelectHighScore(int $gameid = -1, int $id = -1, string $name = null, string $gamename = null, int $score = -1, int $limit = self::baseLimit) {
        $opt = array();
        $whereStr = "WHERE ";

        if($gameid != -1) {
            $whereStr .= "HighScore.gameid = :gameid AND ";
            $opt["gameid"] = $gameid;
        }

        if($id != -1) {
            $whereStr .= "HighScore.id = :id AND ";
            $opt["id"] = $id;
        }

        if($name != null) {
            $whereStr .= "HighScore.name = :name AND ";
            $opt["name"] = $name;
        }

        if($gamename != null) {
            $whereStr .= "HighScoreGames.name = :gamename AND ";
            $opt["gamename"] = $gamename;
        }

        if($score != -1) {
            $whereStr .= "HighScore.score = :score AND ";
            $opt["score"] = $score;
        }


        if($whereStr == "WHERE ")  // Remove the whole where if no where conditions are needed
            $whereStr = "";
        else  // Remove the last "AND " from the where string
            $whereStr = substr($whereStr, 0, -4);

        $res = $this->CallStatement("SELECT
                        HighScore.id AS ID, 
                        HighScore.name AS PlayerName, 
                        HighScore.score AS Score, 
                        HighScoreGames.name AS GameName
                    FROM HighScore
                    INNER JOIN HighScoreGames ON HighScore.gameid = HighScoreGames.id
                    {$whereStr}
                    ORDER BY HighScore.Score DESC
                    LIMIT {$limit};
                    ",
                    $opt
                );
        
        return $res;
    }

    // INSERT
    function InsertHighScore(string $name, int $score, int $gameid): mixed
    {
        $res = $this->CallStatement(
            "INSERT INTO HighScore (HighScore.name, HighScore.score, HighScore.gameid)
            VALUES (:name, :score, :gameid);",
            array(
                "name" => $name,
                "score" => $score,
                "gameid" => $gameid
            )
        );

        return $res;
    }


    /// HighScore Games ///
    // SELECT
    function SelectGame(int $id = -1, string $key = null, string $name = null, int $limit = self::baseLimit) {
        $opt = array();
        $whereStr = "WHERE ";

        if($id != -1) {
            $whereStr .= "HighScoreGames.id = :id AND ";
            $opt["id"] = $id;
        }

        if($name != null) {
            $whereStr .= "HighScoreGames.name = :name AND ";
            $opt["name"] = $name;
        }

        if($key != null) {
            $whereStr .= "HighScoreGames.key = :key AND ";
            $opt["key"] = $key;
        }

        if($whereStr == "WHERE ")  // Remove the whole where if no where conditions are needed
            $whereStr = "";
        else  // Remove the last "AND " from the where string
            $whereStr = substr($whereStr, 0, -4);

        $res = $this->CallStatement("SELECT
                HighScoreGames.id AS ID,
                HighScoreGames.name AS GameName
            FROM HighScoreGames
            {$whereStr}
            LIMIT {$limit};
            ",
            $opt
        );

        return $res;
    }

    // INSERT
    function InsertGame($gameName, $gameKey): mixed
    {
        $res = $this->CallStatement(
            "INSERT INTO HighScoreGames (HighScoreGames.name, HighScoreGames.key)
            VALUES (:name, :key);",
            array(
                "name" => $gameName,
                "key" => $gameKey
            )
        );

        return $res;
    }
}
?>