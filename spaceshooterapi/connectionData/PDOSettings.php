<?php namespace HighscoreAPI;
require_once(__DIR__ . "/../manager.php");
MakeInaccessible(__FILE__);

use PDO;

class PDOSettings {
    public string $host;
    public string $user;
    public string $pwd;
    public string $db;

    public string $dsn;

    public array $options;

    function __construct(string $host, 
    string $user,
    string $pwd,
    string $db,
    array $options = [ // DEFAULT OPTIONS VALUES
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES,
        false
    ])
    {
        $this->host = $host;
        $this->user = $user;
        $this->pwd = $pwd;
        $this->db = $db;

        $this->dsn = "mysql:host={$host};dbname={$db}";

        $this->options = $options;
    }
}

?>