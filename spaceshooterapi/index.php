<?php namespace HighscoreAPI;
require_once(__DIR__ . "/manager.php");
require_once(__DIR__ . "/common/headers.php");

$gameNameError = Error($_GET["nameError"] ?? "");
$keyError = Error($_GET["keyError"] ?? "");

unset($_GET["nameError"]);
unset($_GET["keyError"]);

$htmlContents = <<<EOD
<div>
    <h2 class="text-4xl">Register a new game</h2>
    <form method="POST" action="./logic/registerGame.php" autocomplete="off">
        <p class="text-xl">
            <input class="p-1" required type="text" name="gameName" placeholder="Game Name" autocomplete="off">
        </p>
        {$gameNameError}

        <p class="text-xl">
            <input class="p-1" required type="password" name="registerKey" placeholder="Register Key" autocomplete="off">
        </p>
        {$keyError}

        <input class="text-xl px-1 rounded" type="submit" name="submit" value="Register">
    </form>
</div>
EOD;

$title = "Register new game";
$header = $startingHeader;
$bodyclasses = "";
$footer = "";

require("./page.php");
exit;
?>