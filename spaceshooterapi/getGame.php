<?php namespace HighscoreAPI;
require_once(__DIR__ . "/manager.php");
require_once(__DIR__ . "/connection.php");

$res = array();

$limit = PDOExtend::baseLimit;
if(isset($_GET["limit"]))
    $limit = intval(trim($_GET["limit"]));

$idvar = $_GET["id"];
$id = -1;
if(isset($idvar) && is_numeric($idvar))
    $id = intval($idvar);

$namevar = $_GET["name"];
$name = null;
if(isset($namevar))
    $name = $namevar;

$keyvar = $_GET["key"];
$key = null;
if(isset($keyvar))
    $key = $keyvar;


$res = $pdo->SelectGame(
    id: $id, 
    key: $key, 
    name: $name, 
    limit: $limit);

    
$res = json_encode($res);
print($res);
exit;
?>