<?php namespace HighscoreAPI;
require_once(__DIR__ . "/../manager.php");
MakeInaccessible(__FILE__);

$generalError = "Something went wrong, please try again later!";

$gameNameExistsError = "This game name already exists!";

$gameNameNotInFormat = "The game name is not in the correct format! (1 - 30 letters, no spaces)!";

$registerKeyNotMatch = "The register key you send doesn't match!";
?>