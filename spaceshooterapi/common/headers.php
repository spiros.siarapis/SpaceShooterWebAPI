<?php namespace HighscoreAPI;
require_once(__DIR__ . "/../manager.php");
MakeInaccessible(__FILE__);

function baseHeader(string $content): string
{
    $baseHeader = <<<EOD
    <header class="w-full bg-black px-5 py-3 flex flex-row justify-between">
        {$content}
    </header>
    EOD;

    return $baseHeader;
}


$loggedInHeader = baseHeader(
    <<<EOD
    <h1 class="text-4xl text-white">Highscore API</h1>
    <nav class="">
        <a class="text-3xl text-white no-underline mx-2" href="./main.php">Main</a>
        <a class="text-3xl text-white no-underline mx-2" href="./settings.php">Settings</a>
        <a class="text-3xl text-white no-underline mx-2" href="./logic/logout.php">Log Out</a>
    </nav>
    EOD
);

$startingHeader = baseHeader(
    <<<EOD
    <h1 class="text-4xl text-white">Highscore API</h1>
    EOD
);
?>