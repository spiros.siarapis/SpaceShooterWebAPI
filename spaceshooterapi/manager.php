<?php namespace HighscoreAPI;
// Make file inaccessible
function MakeInaccessible(string $current_file) {
    if (basename($current_file) == basename($_SERVER["SCRIPT_FILENAME"])) {
        // Send a forbidden http response code
        http_response_code(403);
    }
}

MakeInaccessible(__FILE__);

// Verify that name has the correct format
// Not empty, no spaces, at least 1 character and at most 30
function NameVerify(string $name): bool
{ 
    if(!empty($name) && strpos($name, ' ') === false && 0 < strlen($name) && strlen($name) <= 30)
        return true;
    
    return false;
}


// Verify that the score has the correct format
// Not null, is numeric, more than 0
function ScoreVerify(string $score)
{
    if(isset($score) && is_numeric($score) && intval($score) > 0)
        return true;
    
    return false;
}

// How the error looks like in html
function Error(string $errorMessage): string
{
    $error = <<<EOD
    <p class="text-xl text-red-400">
        {$errorMessage}
    </p>
    EOD;

    return $errorMessage != "" ? $error : "";
}

// Key to be able to register a new game
// Needs to be saved in a secure place
// key: 8uQc^Fyd#aobbtJZThKfk&@w@M6vmVvmr2v3kjae
$registerKey = '$2y$10$cXKukbAdPF7RIIxJa0cBx.148mlK6.hv2C3qjO2nFI6VaoPtTvJAq';


// Cryptographically Secure Pseudo-Random Number Generator //
// https://stackoverflow.com/questions/4356289/php-random-string-generator
/**
 * Generate a random string, using a cryptographically secure 
 * pseudorandom number generator (random_int)
 *
 * This function uses type hints now (PHP 7+ only), but it was originally
 * written for PHP 5 as well.
 * 
 * For PHP 7, random_int is a PHP core function
 * For PHP 5.x, depends on https://github.com/paragonie/random_compat
 * 
 * @param int $length      How many characters do we want?
 * @param string $keyspace A string of all possible characters
 *                         to select from
 * @return string
 */
function random_str(
    int $length = 64,
    string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*'
): string
{
    if ($length < 1) {
        throw new \RangeException("Length must be a positive integer");
    }
    
    $pieces = [];
    
    $max = mb_strlen($keyspace, '8bit') - 1;
    
    for ($i = 0; $i < $length; ++$i) {
        $pieces []= $keyspace[random_int(0, $max)];
    }

    return implode('', $pieces);
}
?>