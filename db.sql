-- CREATE THE HIGHSCOREGAMES TABLE --
CREATE TABLE `HighScoreGames` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `key` varchar(100) NOT NULL
);

-- INDEXES FOR HIGHSCOREGAMES --
ALTER TABLE `HighScoreGames`
  ADD PRIMARY KEY (`id`);

-- AUTO INCREMENT FOR HIGHSCOREGAMES --
ALTER TABLE `HighScoreGames`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


-- CREATE THE HIGHSCORE TABLE --
CREATE TABLE `HighScore` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `score` int(11) NOT NULL,
  `gameid` int(11) NOT NULL
);

-- INDEXES FOR HIGHSCORE --
ALTER TABLE `HighScore`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_game` (`gameid`);

-- AUTO INCREMENT FOR HIGHSCORE --
ALTER TABLE `HighScore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- CONSTRAINT FOR HIGHSCORE --
ALTER TABLE `HighScore`
  ADD CONSTRAINT `fk_game` FOREIGN KEY (`gameid`) REFERENCES `HighScoreGames` (`id`);
